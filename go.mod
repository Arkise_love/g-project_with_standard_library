module g-project_with_standard_library

go 1.22.1

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.6.0
)
